import math



def formRessort(e, v):
	return (1/3)*(e/(1-(2*v)))

def formLongueurAVide(lb, lc):
	tempo = math.pow(lb,2)-math.pow(lc,2)
	if(tempo < 0):
		return 0
	else:
		return (1/2)*math.sqrt(tempo)
	

def formLongueurDuDeplacement(lf, lv):
	return lf-lv

def formMasseProjectil(p,b,h,lf):
	return p*b*h*lf

def formVelocite(k, ld, mp):
	tmp = (k * math.pow(ld, 2)) / mp
	if(tmp>0):
		return math.sqrt(tmp)
	else:
		return 0

def formPorte(v, g, a):
	return ((math.pow(v, 2))/g)*math.sin(2*a)

def formEnergie(mp, v):
	return (1/2)*mp*math.pow(v, 2)




def generationNote(a, lb, b, h, lc, lf, bfle, hfle, v, g, e, p, const_cible):
	
	k = formRessort(float(e), float(v))
	lv = formLongueurAVide(float(lb), float(lc))
	ld = formLongueurDuDeplacement(float(lf), lv)
	mp = formMasseProjectil(float(p), float(bfle) ,float(hfle) ,float(lf))
	velocite = formVelocite(k, ld, mp)

	porterP = formPorte(velocite, float(g), float(a))
	energie = formEnergie(mp, velocite)

	

	distanceDelacible = porterP - const_cible
	

	# Distance mise au positif
	if(distanceDelacible < 0):
		distanceDelacible = distanceDelacible * (-1)
	
	noteDistance = 10
	noteEnergie = 1

	if(distanceDelacible < const_cible):
		noteDistance = math.pow((const_cible-distanceDelacible)*1000,2)*0.01
		noteEnergie = energie*5
	
	note = noteDistance + noteEnergie
	return donneToString(a, lb, b, h, lc, lf, bfle, hfle, v, g, e, p, str(k), str(lv), str(ld), str(mp), str(velocite), str(porterP), str(energie), str(note))

def donneToString(a, lb, b, h, lc, lf, bfle, hfle, v, g, e, p, k, lv, ld, mp, V, d, energ, note):
	return a+ "_" +lb+ "_" +b+ "_" +h+ "_" +lc+ "_" +lf+ "_" +bfle+ "_" +hfle+ "_" +v+ "_" +g+ "_" +e+ "_" +p+ "_" +k+ "_" +lv+ "_" +ld+ "_" +mp+ "_" +V+ "_" +d+ "_" +energ+ "_" +note

	
## LES LIMITES ##

def formMomentQuadratique(b, h):
	return (b*math.pow(h, 3))/12

def formForceTraction(k, lf):
	return k*lf

def formFlecheDuBras(f, lb, e, i):
	return (f*math.pow(lb,3))/(	48*e*i)