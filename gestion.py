import matplotlib.pyplot as plt
import numpy as np 


def variance(population):
	noteTempo = []
	for i in range(len(population)):
		a, lb, b, h, lc, lf, bfle, hfle, v, g, e, p, k, lv, ld, mp, V, d, energ, note = population[i].split("_")
		noteTempo.append(float(note))

	return np.var(noteTempo)

def energie(population):
	maxEnerg = 0

	for i in range(len(population)):
		a, lb, b, h, lc, lf, bfle, hfle, v, g, e, p, k, lv, ld, mp, V, d, energ, note = population[i].split("_")
		if(maxEnerg < float(energ)):
			maxEnerg = float(energ)

	return str(maxEnerg)


def distance(population, CONST_CIBLE):
	minDistance = 99999999
	for i in range(len(population)):
		distance = 0
		a, lb, b, h, lc, lf, bfle, hfle, v, g, e, p, k, lv, ld, mp, V, d, energ, note = population[i].split("_")
		distance = float(d) - CONST_CIBLE
		
		if(distance < 0):
			distance = distance * (-1)
		
		if(minDistance > distance):
			minDistance = distance

	return str(minDistance)

def note(population):
	maxNote = 0
	for i in range(len(population)):
		a, lb, b, h, lc, lf, bfle, hfle, v, g, e, p, k, lv, ld, mp, V, d, energ, note = population[i].split("_")
		if(maxNote < float(note)):
			maxNote = float(note)

	return str(maxNote)


def graphique(varianceTab, generationTab, x1, iteration, energieTab, noteTab, distanceTab):
	
	plt.title("Evolution de la variance")
	
	a1 = plt.subplot(221)
	a1.plot(generationTab, varianceTab)
	a1.set_title("variance")

	#a2 = plt.subplot(222)
	#a2.plot(iteration, x1)
	#a2.set_title("porter du scorpion")
	
	a3 = plt.subplot(223)
	a3.plot(iteration, energieTab)
	a3.set_title("energie")

	a4 = plt.subplot(224)
	a4.plot(iteration, noteTab)
	a4.set_title("note")

	a2 = plt.subplot(223)
	a2.plot(iteration, distanceTab)
	a2.set_title("distance de la cible")


	plt.show()




def graphByGeneration(allGeneration, allVariance, allEnergie, allDistance, allNote):
	
	plt.title("Evolution à travers les générations")
	
	a1 = plt.subplot(221)
	a1.plot(allGeneration, allVariance)
	a1.set_title("Variance")

	a2 = plt.subplot(222)
	a2.plot(allGeneration, allDistance)
	a2.set_title("Distance de la cible (min pour une génération)")
	
	a3 = plt.subplot(223)
	a3.plot(allGeneration, allEnergie)
	a3.set_title("Energie deployée (max pour une génération)")

	a4 = plt.subplot(224)
	a4.plot(allGeneration, allNote)
	a4.set_title("Note donnée (max pour une génération)")

	plt.show()