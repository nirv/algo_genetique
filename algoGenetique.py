import random
import formulescorpion
import gestion

# a 	: angle de la hause (degrÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©e)
# Lb 	: longueur du bras de l'arc (metre)
# b 	: base de la section du bras (metre)
# h 	: hauteur de la section du bras (metre)
# Lc 	: longueur de la corde (metre)
# Lf 	: longueur de la fleche (metre)
# bfle	: base de la fleche
# hfle	: hauteur de la fleche
# v  	: coef poisson : caoutchouc : 0,5
# g 	: gravité
# e 	: val young du matériau
# p 	: masse volumique de la fleche
# K 	: Ressort
# Lv 	: Longuer a vide
# Ld 	: Longueur de deplacement
# mp 	: masse du projectile
# V 	: Velocite
# d 	: porter P
# energ : energie
# note 	: note


CONST_TAILLE_POP 	= 200
CONST_CIBLE 		= 350 # metre
CONST_LETTRE_random = ["a", "lb", "b", "h", "lc", "lf", "bfle", "hfle"]
CONST_LETTRE_formule  = ["v", "g", "e", "p", "k", "lv", "ld", "mp", "V", "d", "energ", "note"]
CONST_GENERATION = 40

#Cette fonction est l'entrÃ© de notre application
# Elle permet donc de
# - generer notre population
# Ensuite nous bouclons (sur le nombre de gÃ©nÃ©ration)
# - Evaluation de la population
# - Selection de la population
# - Croisement de la population
# - Mutation de la population

# Les lignes suivante serve Ã  generer nos graphique
def init():
	i = 0
	allVariance = []
	allGeneration = []
	allEnergie = []
	allDistance = []
	allNote = []

	porterPTab = []
	energieTab = []
	noteTab = []
	distanceTab = []

	alli = []
	allii = 0


	population = initPopulation()
	while CONST_GENERATION > i:

		print("My generation " + str(i))
		population = initEvaluation(population)
		selection  = initSelection(population)
		population = initCroisement(population, selection)
		population = initMutation(population)




		for u in range(len(population)):
			distance = 0
			a, lb, b, h, lc, lf, bfle, hfle, v, g, e, p, k, lv, ld, mp, V, d, energ, note = population[u].split("_")


		#PrÃƒÂ©pa des donnÃƒÂ©es pour le taleau
		variance = gestion.variance(population)
		energieMax = gestion.energie(population)
		DistanceMin = gestion.distance(population, CONST_CIBLE)
		noteMax = gestion.note(population)


		allVariance.append(variance)
		allEnergie.append(energieMax)
		allDistance.append(DistanceMin)
		allNote.append(noteMax)

		allGeneration.append(i)
		i = i + 1

	gestion.graphByGeneration(allGeneration, allVariance, allEnergie, allDistance, allNote)





# Mutation
# Ici nous avons une chance sur 200 de muter quelqu'un
# ensuite random sur CONST_LETTRE_random pour choisir quel donnÃƒÂ©e modifiÃƒÂ©e
def initMutation(population):
	populationMuter = []
	for i in range(len(population)):
		randomPerso = random.randint(0, 200)
		populationMuterTempo = ""
		if(randomPerso == 1):
			ranCONST_Letre = random.randint(0, len(CONST_LETTRE_random)-1)
			popsplit = population[i].split("_")
			for u in range(len(popsplit)):
				if(ranCONST_Letre == u):
					if(u != len(popsplit)-1):
						populationMuterTempo = populationMuterTempo + genereDonne(CONST_LETTRE_random[u]) + "_"
					else:
						populationMuterTempo = populationMuterTempo + genereDonne(CONST_LETTRE_random[u])

				else:
					if(u != len(popsplit)-1):
						populationMuterTempo = populationMuterTempo + popsplit[u] + "_"
					else:
						populationMuterTempo = populationMuterTempo + popsplit[u]

		if(populationMuterTempo != ""):
			populationMuter.append(populationMuterTempo)
		else:
			populationMuter.append(population[i])

	return populationMuter



# Ici notre croisement focntionne avec la selection fait prÃƒÂ©cÃƒÂ©dement
# Nous gÃƒÂ©nÃƒÂ©rons une nombre random sur les valeurs a croisÃƒÂ©e (CONST_LETTRE_random) (voir la variable declarer plus haut)
# Donc nous boucle avec un pas de 2 (pour papa 1 et papa 2)
# Croisement sur le random pour gÃƒÂ©nÃƒÂ©rer 2 nouvelles personnes
def initCroisement(population, selection):

	populationEnfant = []
	pop1 = []
	pop2 = []
	popEnfant1 = []
	popEnfant2 = []

	tailleTab = len(population[0].split("_"))

	#liste de la selection en tab
	maSelection = selection.split("_")
	for i in range(0, len(maSelection), 2):

		tailleTempoTab = len(CONST_LETTRE_random)
		perc = random.randint(0, tailleTempoTab)

		popEnfant1 = ""
		popEnfant2 = ""

		lenSelect1 = int(maSelection[i])
		lenSelect2 = int(maSelection[i+1])
		pop1 = population[lenSelect1].split("_")
		pop2 = population[lenSelect2].split("_")


		for y in range(0, perc):
			popEnfant1 = popEnfant1 + pop1[y] + "_"
			popEnfant2 = popEnfant2 + pop2[y] + "_"

		for z in range(perc, tailleTab):
			if(z != tailleTab-1):
				popEnfant1 = popEnfant1 + pop2[z] + "_"
				popEnfant2 = popEnfant2 + pop1[z] + "_"
			else:
				popEnfant1 = popEnfant1 + pop2[z]
				popEnfant2 = popEnfant2 + pop1[z]

		populationEnfant.append(popEnfant1)
		populationEnfant.append(popEnfant2)

	return populationEnfant





# Selection des BESTS parents
# La selection se fait sur le systeme de roulette (RWS)
# Retourne un string avec les index des personnes choisie sÃƒÂ©parer d'un _
# example : "1_5_8_4_2_1_7"

def initSelection(population):
	# GÃƒÂ©nÃƒÂ©ration d'un Offset maximum avec 1/4 de la totalitÃƒÂ© des notes
	maxOffset = 1
	totalNote = 0
	for k in range(len(population)):
		a, lb, b, h, lc, lf, bfle, hfle, v, g, e, p, k, lv, ld, mp, V, d, energ, note  = population[k].split("_")
		if(float(note) > maxOffset):
			totalNote = totalNote + float(note)

		maxOffset = int(round(totalNote/4,0))

	# Maintenant nous randomisons afin de crÃƒÂ©er l'offset
	offset = random.randint(1, maxOffset)

	tempo = 0
	couple = ""
	nbPop = 0
	offsetFin = 0
	# I love potatoes
	taille = len(population)
	while nbPop < taille:

		for x in range(0, taille):
			if(nbPop < taille):
				a, lb, b, h, lc, lf, bfle, hfle, v, g, e, p, k, lv, ld, mp, V, d, energ, note = population[x].split("_")
				tempo = tempo + float(note)
				if(tempo >= offset):
					if(couple != ""):
						couple = couple + "_" + str(x)
						nbPop = nbPop + 1
					else:
						couple = str(x)
						nbPop = nbPop + 1

					offset = offset + offset
				if(x == (taille-1)):
					offset = 0-(tempo - offset)

	return couple




# pour une population nous allons passer tous les "personnes" afin de les ÃƒÂ©valuers
# Modifie la valeurs "note" contenue dans une "personne" de la population
def initEvaluation(population):

	populationEvaluer = []

	for i in range(len(population)):
		a, lb, b, h, lc, lf, bfle, hfle, v, g, e, p, k, lv, ld, mp, V, d, energ, note = population[i].split("_")
		unPopulation = formulescorpion.generationNote(a, lb, b, h, lc, lf, bfle, hfle, v, g, e, p, CONST_CIBLE)
		populationEvaluer.append(unPopulation)
	return populationEvaluer


# genere la population et vÃƒÂ©rifie les limites
# Nous prÃƒÂ©fÃƒÂ©rons tirer une autre fois (la boucle while) afin d'avoir des courbes plus intÃƒÂ©rÃƒÂ©ssante
def initPopulation():
	population = []
	for i in range(0, CONST_TAILLE_POP):
		stop = 0
		while stop == 0:

			a 	= genereDonne("a")
			lb 	= genereDonne("lb")
			b 	= genereDonne("b")
			h 	= genereDonne("h")
			lc 	= genereDonne("lc")
			lf 	= genereDonne("lf")
			bfle= genereDonne("bfle")

			hfle= genereDonne("hfle")
			v 	= genereDonne("v")
			g 	= genereDonne("g")
			e 	= genereDonne("e")
			p 	= genereDonne("p")
			k 	= str(0)
			lv 	= str(0)
			ld 	= str(0)
			mp 	= str(0)
			V 	= str(0)
			d 	= str(0)
			energ= str(0)
			note= str(0)

			stop= verificationDesLimite(b, h, e, v, k, lf, lb, lc)
		if(stop != 0):
			population.append(donneToString(a, lb, b, h, lc, lf, bfle, hfle, v, g, e, p, k, lv, ld, mp, V, d, energ, note))

	return population



#Fonction pour la concatenation des valeurs de notre scorpion
def donneToString(a, lb, b, h, lc, lf, bfle, hfle, v, g, e, p, k, lv, ld, mp, V, d, energ, note):
	return a+ "_" +lb+ "_" +b+ "_" +h+ "_" +lc+ "_" +lf+ "_" +bfle+ "_" +hfle+ "_" +v+ "_" +g+ "_" +e+ "_" +p+ "_" +k+ "_" +lv+ "_" +ld+ "_" +mp+ "_" +V+ "_" +d+ "_" +energ+ "_" +note

# Gere les limites d'un scorpion
# return 1 si ok sinon 0
def verificationDesLimite(b, h, e, v, k, lf, lb, lc):

	i = formulescorpion.formMomentQuadratique(float(b), float(h))
	k = formulescorpion.formRessort(float(e), float(v))
	forceTra = formulescorpion.formForceTraction(k, float(lf))
	f = formulescorpion.formFlecheDuBras(forceTra, float(lb), float(e), i)

	lv = formulescorpion.formLongueurAVide(float(lb), float(lc))
	ld = formulescorpion.formLongueurDuDeplacement(float(lf), lv)

	#return 1
	if(ld > float(f)):
		return 0
	else:
		if(lv > float(lf)):
			return 0
		else:
			if(float(lc) > float(lb)):
				return 0
			else:
				return 1


# Cette fonction permet de renvoyer des donnÃ©e alÃ©atoire (avec un parametre ce que l'on cherche)
def genereDonne(lettre):
	if(lettre == "a"):
		return str(randomPerso(0, 90, 0))		# a 	angle
	elif(lettre == "lb"):
		return str(randomPerso(2, 50, 0)) 		# Lb 	longueur bras scorpion
	elif(lettre == "b"):
		return str(randomPerso(0.01, 1, 2))		# b  	section base du bras
	elif(lettre == "h"):
		return str(randomPerso(0.01, 1, 2))		# h 	section hauteur du bras
	elif(lettre == "lc"):
		return str(randomPerso(1, 40, 0))		# Lc 	longueur corde
	elif(lettre == "lf"):
		return str(randomPerso(4, 23, 0))		# Lf 	longueur fleche
	elif(lettre == "v"):
		return str(0.02) 						# v
	elif(lettre == "g"):
		return str(9.81)						# g
	elif(lettre == "e"):
		return str(210)							# E
	elif(lettre == "p"):
		#return str(7850)						# p
		return str(randomPerso(600, 100, 0))
	elif(lettre == "bfle"):
		return str(randomPerso(0.01, 0.5, 2))	# bfle	section de base de la fleche (fleche carrÃƒÂ© oui oui)
	elif(lettre == "hfle"):
		return str(randomPerso(0.01, 0.5, 2))	# hfle 	section de hauteur d'une fleche (fleche carrÃƒÂ©)

# Cette fonction permet de renvoyer un nombre alÃ©atoire
# EntrÃ©e :
# 	debut : valeur minimum du random
# 	fin : valeur max du random
# 	nbApresVirgule : nombre de chiffre apres la virgule de notre chiffre random
def randomPerso(debut, fin, nbApresVirgule):
	return round(random.uniform(debut, fin), nbApresVirgule)





